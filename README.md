# Instructions for dotfiles  
  
> Download zip [here](https://gitlab.com/qash/dotfiles/-/archive/master/dotfiles-master.zip)  
> Make sure you have the programs you want to configure installed  
> Copy wanted files from repo to home  
