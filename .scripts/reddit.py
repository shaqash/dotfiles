#!/usr/bin/env python

import sys
import webbrowser

if len(sys.argv) > 1:
	for i in range(1,len(sys.argv)):
		webbrowser.open('http://www.reddit.com/r/' + str(sys.argv[i]))
else:
	webbrowser.open('http://www.reddit.com')
