#!/bin/bash


killall compton

compton -f -i 0.8 -I 0.05 -b --mark-ovredir-focused --focus-exclude 'FORCE_OPAQUE:s'
