# .scripts
  
## comptonlaunch.sh
Kills all the current proccess of compton and starts a new one.  
  
## iso2usb.sh
Use it to make a bootable usb.
Run it with: ./iso2usb.sh PATHTOISO DRIVE (ex. ~/Downloads/ubuntu1804.iso sdd).  
  
## performanceMode.sh
Changes cpu gov. to Performance.  
  
## polybarrun.sh
Like compton, kills and starts a new one. Using profile 'example', that you can find inside the config in this repository.  
  
## updateHosts.sh
Use it to block unwanted domains.  
It downloads the host file from https://someonewhocares.org/hosts and adds it to your own. It will update again only if the files differ.  
  
## updateHostsNonInteractive.sh
Same as updateHost.sh, only it doesn't involve user interaction. Made so you could run it with schedulers like cron.  
