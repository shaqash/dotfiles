#!/bin/bash

if [ -z "$1" ] || [ -z "$2" ]
then
	echo "Missing argument/s"
	exit 1;
fi
echo "Moving iso: $1 to Drive: $2.."
echo "EXECUTING: sudo dd bs=4M if=$1 of=/dev/$2 conv=fdatasync"
read -p "Confirm? (y/n)?" choice
case "$choice" in
  	y|Y) sudo -S dd bs=4M if=$1 of=/dev/$2 conv=fdatasync;;
  	n|N) echo "Operation cancelled by user.";;
  	*) echo "invalid";;
esac
