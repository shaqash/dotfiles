#!/bin/bash

a=$(eopkg lu)

if [ "$a" != "No packages to upgrade." ]; then
    eopkg lu | wc -l
else
    echo $1
fi