#!/bin/bash

i3status | while :
do
	read line
	echo $(cat /proc/$(xdotool getwindowpid \
    $(xdotool getwindowfocus))/comm) \
    $(echo "[$USER]") \
    $(echo "  VOL:") \
    $(amixer sget Master | grep 'Right:' | awk -F'[][]' '{ print $2 }') \
    $(echo " CPU:") \
    $(top -bn1 | grep "Cpu(s)" | \
           sed "s/.*, *\([0-9.]*\)%* id.*/\1/" | \
           awk '{print 100 - $1"%"}') \
    $(echo "  ") \
    $(date) \
    || exit 1
done
